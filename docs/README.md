---   
home: false
heroImage: /logo.png
actionText: 快速上手 →
actionLink: /guide/introduce
features:
- title: 强大的代码生成
  details: 整合CodeRd，从前端生成到数据库，支持在线生成和命令式生成。
- title: 可定制化功能模块
  details: 支持部门管理、岗位管理、服务监测、字典管理等模块的定制（全部免费）。
- title: 可定制化技术栈
  details: 前后端均支持技术栈定制，如可选的缓存策略，可选的权限框架等。  
- title: 不止于极速开发，更是规范
  details: 伊娃（Eva）不仅仅是一套极速开发的管理系统框架，更是一套助你养成良好代码规范的良师益友。
- title: 易上手 & 文档齐全
  details: 代码经过合理、安全的封装，让新手也能快速上手。齐全的文档和视频教程让你不畏惧问题。
- title: 干净 & 合理
  details: 你要什么，便是什么！定制化代码通过物理生成，不会冗余各种方案，但你仍然可以在项目后期随意切换。
- title: 基础模块BUG在线修复
  details: 一旦我们得到了BUG，我们会开发BUG修复模版，你只需要执行一句命令即可修复。
- title: 智能跟踪操作日志
  details: 操作日志默认情况下使用智能跟踪模式，避免日志遗漏。当然，我们也提供了手动模式，只需要在接口上增加一个注解即可。
- title: 多模块数据权限
  details: 很多时候我们需要对多个模块进行数据权限的控制，Eva经过简单的封装，让你的数据权限更清晰。
- title: 多种缓存方案无缝升级
  details: 当你的项目对缓存要求越来越高，你可以从伊娃内存式缓存无缝升级到redis高可用或其它缓存方案。  
- title: 按钮粒度的权限控制
  details: 无论你选择的是Shiro还是其它权限框架，均实现了按钮粒度的权限控制。
- title: 会话共享
  details: 会话存储在缓存中，你可以使用负载均衡实现更高的并发而无需关心用户信息丢失或不同步的问题。
- title: 前后端分离
  details: 前后端分离模式！你既可以一人孤独求败，也可以与同事一起划水摸鱼。
footer: MIT Licensed | Copyright © 2018-2021 eva.adjust-rd.com All Rights Reserved 伊娃（Eva）
---
