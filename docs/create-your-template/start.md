# 开始制作

## Hello World

### 一、打开CodeRd IDE

点击网站右上角的`自定义模版`按钮可打开CodeRd IDE。

### 二、填写模版信息
![设置](/doc/create-your-template/setting.png)

### 三、新建index.ftl文件
![创建文件](/doc/create-your-template/createFile.png)
![创建文件](/doc/create-your-template/createFile2.png)

编写代码如下

```freemarker
Hello CodeRd
```
### 四、运行

点击右上角`运行`按钮打开执行器，打开后如下图
![执行器](/doc/create-your-template/runner.png)

点击生成代码按钮，可以看到以下输出（如提示请选择项目可自行创建一个新项目，默认会选中您的第一个项目）
![生成结果](/doc/create-your-template/genResult.png)

到这里，我们就完成了第一个CodeRd模版。

## 使用变量

我们希望生成结果不是静态内容，所以我们需要在模版中使用变量来使生成结果动态起来。

使用变量的基本语法是`${变量名称}`。

编写以下代码至文件中。

```freemarker
大家好，我是${_author}
我正在使用“${_project.zhName}”项目生成代码
生成时间：${_yyyy}/${_MM}/${_dd} ${_HH}:${_mm}:${_ss}
```

运行后将输出以下结果

![生成结果](/doc/create-your-template/genResultVariable.png)

代码中的`_author`, `_project.zhName`为CodeRd提供的全局属性变量，更多属性请查看[全局属性](/doc/create-your-template/attrs)。

## 自定义全局属性

通过使用变量可以将生成结果动态起来，当CodeRd提供的全局属性变量无法满足需求时，我们可以通过自定义属性来完成。这里讲述自定义全局属性，所谓全局指的是所有的模版文件都可以使用。下图定义了一个全局属性。
![生成结果](/doc/create-your-template/definedGlobalVariable.png)

通过属性定义时指定的变量来获取属性的值。得出代码如下：
```freemarker
${globalVariable}
```

运行后执行器中将出现我们自定义的全局属性，如下
![生成结果](/doc/create-your-template/definedGlobalVariablePreview.png)

需要注意的是，虽然全局属性中可以使用`_table`等由系统提供的全局属性，但`_table`是数据周期属性，这会导致您所有的模版文件都需要成为数据周期的文件。因为每个模版文件在运行时都会编译并加载全局属性。

::: tip 后定义的属性可以访问前面的属性
我们知道后定义的变量可以访问前面已定义的变量，像下面这样：
```javascript
var a = 1; var b = a; 
```
同理，全局属性也满足这点。
:::

## 自定义逻辑展示的全局属性
就像一件商品只有打折时才要求输入折扣比例一样，某些全局属性有时候也希望在满足一定的条件下才要求输入/选择。下图我们模拟设定商品折扣的场景定义了两个属性。
![生成结果](/doc/create-your-template/logicGlobalVariable.png)

蓝色标记处始终勾选，则表示属性始终被展示。点击右边的设置按钮，打开逻辑设置面板，在面板中编写代码如下：
```freemarker
${hasRatio?c}
```
**注意**：逻辑值必须返回'true'或'false'字符串，当逻辑值的运行结果为'true'时表示该属性需要被展示。

## 使用数据模型

使用数据模型可以让我们与数据库表关联起来。在使用数据模型之前你需要启用模版的数据模型，并且将文件的生成周期调整为数据周期。你可以按照以下步骤进行。

**启用模版数据模型**

![启用模版数据模型](/doc/create-your-template/enableDataModel.png)

**模版文件生成周期调整为数据周期**

![调整为数据周期](/doc/create-your-template/changeCycle.png)

**编写代码**

以下代码获取数据表的驼峰命名名称。

```freemarker
${_table.camelCaseName}
```

**运行**

点击运行，选择数据模型

![选择数据模型](/doc/create-your-template/selectDataModel.png)

生成后如下

![生成结果](/doc/create-your-template/genByDataModelPreview.png)

## 使用关联模型

使用关联模型之前，你需要启用模版的关联模型，并且项目中包含两个以上数据模型（用于构建关联模型）。你可以按照以下步骤进行。

**启用关联模型**

![启用关联模型](/doc/create-your-template/enableRelationModel.png)

**模版文件生成周期调整为数据周期**

![调整为数据周期](/doc/create-your-template/changeCycle.png)

**编写代码**

以下代码获取数据表的驼峰命名名称。

```freemarker
主表名称：${_table.master.name}
关联表数量：${_table.relations?size}
```

**运行**

点击运行按钮后，点击数据模型/配置项目数据模型，为一个数据模型添加添加关联模型，如下图

![配置数据模型](/doc/create-your-template/createRealtionModel.png)
![配置数据模型](/doc/create-your-template/configRelationModel.png)

选择数据模型

![选择数据模型](/doc/create-your-template/selectRelationModel.png)

生成后如下

![生成结果](/doc/create-your-template/genByDataModelPreview2.png)

## 扩展数据模型字段

有时候我们希望可以增强数据模型中的字段配置，如生成页面时我们希望配置字段是否在列表中展示，生成接口时我们希望可以配置字段的查询方式等。那么你可以像下图这样配置：

![配置数据模型](/doc/create-your-template/configDataModel.png)

## 模版文件的启用与禁用

有时候我们希望某些模版在某种特定的情况下才生效，那么你可以像下图这样进行配置：

![配置模版](/doc/create-your-template/settingFile.png)
![配置模版](/doc/create-your-template/settingFile2.png)

只有在启用逻辑返回'true'字符串时该模版才会生效。

## 模版的验证

编写一个好用的模版，通常需要验证模版的生成条件，如是否选择了数据模型等。无论你需要验证什么，你都可以通过`check`指令来完成。而验证这项任务，你应该交给.cgcheck文件（或以.cgcheck开头的文件）。如下

![配置模版](/doc/create-your-template/check.png)

check指令基本语法如下

```freemarker
<@check message='错误消息' condition='${条件表达式}'/>
```

当条件表达式的结果为'true'字符串时表示验证通过，相反，非'true'字符串时表示验证失败。

## 模版的复用

当多个模版存在相同逻辑时，我们可以将相同的部分抽离成一个公用的模版。然后通过`fork`指令完成复用。如下

![模版的复用](/doc/create-your-template/fork.png)

fork指令的基本语法如下

```freemarker
<@fork alias='模版文件别名'/>
```
模版文件别名可以在模版文件设置中查看。


## 模版的引用

当我们在一个模版中需要获取另一个模版的数据时，我们可以通过获取模版的引用来完成。如下

![模版的引用](/doc/create-your-template/import.png)

import指令的基本语法如下

```freemarker
<@import alias='模版文件别名' var='自定义变量'/>
```

::: warning 请勿在构建周期的模版中引用数据周期的模版
因为数据周期的模版会产生多个生成结果，而构建周期模版只会产生一个生成结果，所以无法在构建周期的模版引用数据周期的模版。
:::
