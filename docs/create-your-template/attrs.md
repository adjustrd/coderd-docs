# 系统属性

使用技巧：在CodeRd中，全局属性变量均以下划线开头。所以，自定义的属性不以"_"开头可以更好的区分。

## 作者

| 属性名称 | 属性变量 | 属性类型 | 所在周期 | 作用域 | 描述
|----|----|----|----|----|----|
| 作者 | _author | String | 数据周期、构建周期 | 全局属性、模版代码、模版属性、属性选项 | 优先使用【代码作者】，如果没有则使用【用户昵称】|
| 作者邮箱 | _email | String | 数据周期、构建周期 | 全局属性、模版代码、模版属性、属性选项 | |
| 作者手机号码 | _mobile | String | 数据周期、构建周期 | 全局属性、模版代码、模版属性、属性选项 | |

## 项目

| 属性名称 | 属性变量 | 属性类型 | 所在周期 | 作用域 | 描述
|----|----|----|----|----|----|
| 项目英文名称 | _project.enName | String | 数据周期、构建周期 | 全局属性、模版代码、模版属性、属性选项 | |
| 项目中文名称 | _project.zhName | String | 数据周期、构建周期 | 全局属性、模版代码、模版属性、属性选项 | |
| 项目描述 | _project.description | String | 数据周期、构建周期 | 全局属性、模版代码、模版属性、属性选项 | |

## 文件

| 属性名称 | 属性变量 | 属性类型 | 所在周期 | 作用域 | 描述
|----|----|----|----|----|----|
| 文件路径 | _filePath | String | 数据周期、构建周期 | 模版代码 | 代码文件最终路径，不包含文件名 |
| 文件名称 | _fileName | String | 数据周期、构建周期 | 模版代码 | 代码文件名称，不包含文件后缀 |
| 文件全名称 | _fullFileName | String | 数据周期、构建周期 | 模版代码 | 代码文件名称，包含文件后缀 |

## 日期

| 属性名称 | 属性变量 | 属性类型 | 所在周期 | 作用域 | 描述
|----|----|----|----|----|----|
| 年份（4位） | _yyyy | String | 数据周期、构建周期 | 全局属性、模版代码、模版属性、属性选项 | |
| 年份（2位） | _yy | String | 数据周期、构建周期 | 全局属性、模版代码、模版属性、属性选项 | |
| 月份 | _MM | String | 数据周期、构建周期 | 全局属性、模版代码、模版属性、属性选项 | |
| 天 | _dd | String | 数据周期、构建周期 | 全局属性、模版代码、模版属性、属性选项 | |
| 周（在当前月中的第几周） | _weekOfMonth | String | 数据周期、构建周期 | 全局属性、模版代码、模版属性、属性选项 | |
| 周（在当前年中的第几周） | _weekOfYear | String | 数据周期、构建周期 | 全局属性、模版代码、模版属性、属性选项 | |
| 时（24H） | _HH | String | 数据周期、构建周期 | 全局属性、模版代码、模版属性、属性选项 | |
| 时（12H） | _hh | String | 数据周期、构建周期 | 全局属性、模版代码、模版属性、属性选项 | |
| 分 | _mm | String | 数据周期、构建周期 | 全局属性、模版代码、模版属性、属性选项 | |
| 秒 | _ss | String | 数据周期、构建周期 | 全局属性、模版代码、模版属性、属性选项 | |
| 毫秒 | _sss | String | 数据周期、构建周期 | 全局属性、模版代码、模版属性、属性选项 | |

## 数据模型

数据模型相关的变量仅在数据周期有效（除_tables外），在构建周期中使用会出现找不到变量的错误。

| 属性名称 | 属性变量 | 属性类型 | 作用域 | 描述
|----|----|----|----|----|
| 数据模型类型 | _table.type | String | 模版代码 | 取值为TABLE和RELATION，TABLE表示单表，RELATION表示关联表 |
| 数据模型名称 | _table.name | String | 模版代码 |  |
| 数据模型别名 | _table.alias | String | 模版代码 | 仅在_table为关联记录或主表时才存在值，否则为null |
| 数据模型原始名称 | _table.originName | String | 模版代码 | 如果是关联模型，则始终等于_table.name |
| 数据模型名称（驼峰命名法） | _table.camelCaseName | String | 模版代码 | 首个单词的首字母小写，其后单词的首字母大写，如"helloWorld" |
| 数据模型名称（帕斯卡命名法） | _table.pascalCaseName | String | 模版代码 | 将单词的首字母全部大写，如"HelloWorld" |
| 数据模型名称（下划线命名法） | _table.underScoreCaseName | String | 模版代码 | 单词通过"_"拼接，如"hello_world" |
| 数据模型名称（中划线命名法） | _table.kebabCaseName | String | 模版代码 | 单词通过"-"拼接，如"hello-world" |
| 数据模型名称 | _table.moduleName | String | 模版代码 | 当数据模型属于某个模块时，该属性表示所属模块名称，否则为null |
| 数据模型备注 | _table.remark | String | 模版代码 |  |
| 数据模型字段数组 | _table.rows | String | 模版代码 |  |
| 数据模型字段名称 | _table.rows[index].name | String | 模版代码 |  |
| 数据模型字段名称（驼峰命名法） | _table.rows[index].camelCaseName | String | 模版代码 |  |
| 数据模型字段名称（帕斯卡命名法） | _table.rows[index].pascalCaseName | String | 模版代码 |  |
| 数据模型字段名称（下划线命名法） | _table.rows[index].underScoreCaseName | String | 模版代码 |  |
| 数据模型字段名称（中划线命名法） | _table.rows[index].kebabCaseName | String | 模版代码 |  |
| 数据模型字段类型 | _table.rows[index].type | String | 模版代码 |  |
| 数据模型字段长度 | _table.rows[index].length | String | 模版代码 |  |
| 数据模型字段小数长度 | _table.rows[index].decimal | String | 模版代码 |  |
| 数据模型字段是否为主键 | _table.rows[index].isPrimaryKey | Boolean | 模版代码 |  |
| 数据模型字段是否自动增长 | _table.rows[index].isAutoIncrement | Boolean | 模版代码 |  |
| 数据模型字段是否必填 | _table.rows[index].required | Boolean | 模版代码 |  |
| 数据模型字段是否唯一 | _table.rows[index].isUnique | Boolean | 模版代码 |  |
| 数据模型字段默认值 | _table.rows[index].defaultValue | String | 模版代码 |  |
| 数据模型字段备注 | _table.rows[index].remark | String | 模版代码 |  |
| 数据模型主键数组 | _table.rows[index].pks | Array | 模版代码 |  |
| 数据模型主表对象 | _table.master | Object | 模版代码 | 拥有与_table保持一致的属性。只有在_table.type为RELATION时才有值。否则为null |
| 数据模型关联记录 | _table.relations | Array | 模版代码 | 拥有与_table保持一致的属性。只有在_table.type为RELATION时才有值。否则为null |
| 数据模型关联记录的映射类型 | _table.relations[index].mapType | String | 模版代码 | 取值o2o表示一对一，o2m表示一对多，m2o表示多对一 |
| 数据模型关联记录的关联类型 | _table.relations[index].relationType | String | 模版代码 | 取值INNER JOIN，LEFT JOIN和RIGHT JOIN |
| 数据模型关联记录的关联条件 | _table.relations[index].condition | String | 模版代码 | 取值INNER JOIN，LEFT JOIN和RIGHT JOIN |
| 所有数据模型 | _tables | Array | 模版代码 |  |
