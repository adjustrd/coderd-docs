# 开始

为了使得生成代码更加的方便，CodeRd提供了同名命令`coderd`。通过命令你可以直接生成代码到项目中。

### 安装

#### 一、安装node
由于coderd发布在npm中，所以在安装coderd前你需要安装node环境。[前往NODE官网下载安装包](https://nodejs.org/zh-cn/download/)

#### 二、安装coderd
```bash
npm install -g coderd
```

#### 三、验证

```bash
coderd
```

出现下图表示安装成功

![安装校验](/doc/command/installCheck.png)
