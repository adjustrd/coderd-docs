module.exports = {
  base: '/doc/',
  title: 'CodeRd 教程文档',
  home: false,
  // head: [
  //   ['link', { rel: 'icon', href: '/favicon.png' }]
  // ],
  description: 'CodeRd教程,CodeRd官方教程',
  markdown: {
    lineNumbers: true,
    toc: {
      includeLevel: [1, 2, 3],
    },
  },
  themeConfig: {
    navbar: true,
    // logo: '/favicon.png',
    sidebarDepth: 2,
    // displayAllHeaders: true, // 是否自动展开所有菜单
    sidebar: [
      {
        title: '开始',   // 必要的
        collapsable: false, // 可选的, 默认值是 true,
        sidebarDepth: 1,    // 可选的, 默认值是 1
        children: [
          '/guide/introduce',
          '/guide/start',
          '/guide/vip'
        ]
      },
      {
        title: '自定义模版',   // 必要的
        collapsable: false, // 可选的, 默认值是 true,
        sidebarDepth: 1,    // 可选的, 默认值是 1
        children: [
          '/create-your-template/what',
          '/create-your-template/start',
          '/create-your-template/attrs',
          // '/create-your-template/data-model',
        ]
      },
      {
        title: '使用命令',   // 必要的
        collapsable: false, // 可选的, 默认值是 true,
        sidebarDepth: 1,    // 可选的, 默认值是 1
        children: [
          '/command/start',
          '/command/common'
        ]
      }
    ],
    nav: [
      { text: '模版库', link: 'http://coderd.adjustrd.com/repositories' },
      { text: 'Eva官网', link: 'http://eva.adjustrd.com' },
      // { text: '在线演示', link: 'http://119.3.126.64:10086' },
      // { text: 'GITEE', link: 'https://gitee.com/coderd-repos/eva' },
      // { text: 'GITHUB', link: 'https://github.com/coderd-repos/eva' },
    ]
  }
}
